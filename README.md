# 3D-Print-Course | Cap. 1 - Diseño y modelado [Básico]

[![3D-Print-Course | Cap. 1 - Diseño y modelado [Básico]](https://img.youtube.com/vi/huoCBsBQ2MQ/0.jpg)](https://youtu.be/huoCBsBQ2MQ)



# Software de diseño 3d 

Hola y bienvenido al primer video de la capacitación de impresión 3d dirigida por el Ing. Andrés Valle y dictada por el estudiante de Ingeniería Jhonatan Peña para el grupo de investigación MAGMA ingeniería de la universidad del Magdalena.

Para iniciar veremos algunas de los software mas utilizados a día de hoy para el diseño y modelado en 3D, luego elegiremos una (de las mas básicas) para empezar con el proceso de creación de una pieza en 3d, en este proceso veremos el uso de herramientas básicas y por ultimo veremos como exportar nuestro archivo para impresión 3D.

A continuación haremos mención de 

1. **Blender**: Blender es un software de modelado 3D **gratuito y de código abierto** que ofrece una amplia gama de funciones. Es adecuado tanto para principiantes como para profesionales. Puedes crear modelos, animaciones y efectos visuales con Blender.
2. **Autodesk Maya**: Maya es un completo software de gráficos por ordenador en 3D utilizado por profesionales. Con Maya, puedes crear modelos, animaciones y efectos visuales de alta calidad. Es especialmente popular en la industria cinematográfica.
3. **Fusion 360**: Fusion 360, desarrollado por Autodesk, es una herramienta completa que combina modelado 3D, diseño paramétrico, simulación y fabricación. Es ampliamente utilizado en ingeniería y diseño industrial. (Es de de pago pero como estudiantes de la universidad del magdalena tenemos licencia institucional para usarlo)
4. **SketchUp**: SketchUp es conocido por su interfaz intuitiva y es popular entre arquitectos y diseñadores. Es excelente para crear modelos arquitectónicos y visualizaciones.
5. **Tinkercad**: Tinkercad es una herramienta de modelado 3D en línea diseñada para principiantes y educadores. Es fácil de usar y es ideal para proyectos simples y educativos.